-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2016 at 02:36 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sis`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE IF NOT EXISTS `absen` (
  `id_absen` int(3) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `id_kelas` int(3) NOT NULL,
  `masuk` int(1) NOT NULL,
  `sakit` int(1) NOT NULL,
  `izin` int(1) NOT NULL,
  `alfa` int(1) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absen`
--

INSERT INTO `absen` (`id_absen`, `nis`, `id_kelas`, `masuk`, `sakit`, `izin`, `alfa`, `tanggal`) VALUES
(1, '27', 1, 0, 0, 0, 1, '2016-10-06'),
(2, '28', 1, 1, 0, 0, 0, '2016-10-06'),
(3, '29', 1, 0, 1, 0, 0, '2016-10-06'),
(10, '27', 1, 0, 0, 0, 1, '2016-10-07'),
(11, '28', 1, 1, 0, 0, 0, '2016-10-07'),
(12, '29', 1, 1, 0, 0, 0, '2016-10-07');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `artikel_id` int(11) NOT NULL,
  `artikel_judul` varchar(255) NOT NULL,
  `artikel_isi` text NOT NULL,
  `artikel_tgl` datetime NOT NULL,
  `kategori_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`artikel_id`, `artikel_judul`, `artikel_isi`, `artikel_tgl`, `kategori_id`) VALUES
(51, 'aa', '<span>aa<br></span>', '2015-04-29 20:45:44', 3),
(58, 'asda', 'Buku GRATIS !!!<span> Teman-teman saya mau bagi buku gratis nih, syarat nya:<br> 1. Like dan share status ini<br> 2. Invite pin saya 53DEA7F1<span><br> 3. Silakan tulis di kolom komentar dengan impian terbesar teman-teman</span></span> Karena buku nya tinggal satu pcs, maka komentar terakhir yang mendapatkan like dari saya yang akan mendapatkan buku ini. Pemenang akan saya umumkan besok siang<br> Selamat mencoba, semoga anda beruntung <br>', '2015-04-29 01:11:06', 1),
(59, 'Asdasdwdcadscacasc', 'Buku GRATIS !!!<span> Teman-teman saya mau bagi buku gratis nih, syarat nya:<br> 1. Like dan share status ini<br> 2. Invite pin saya 53DEA7F1<span><br> 3. Silakan tulis di kolom komentar dengan impian terbesar teman-teman</span></span> Karena buku nya tinggal satu pcs, maka komentar terakhir yang mendapatkan like dari saya yang akan mendapatkan buku ini. Pemenang akan saya umumkan besok siang<br> Selamat mencoba, semoga anda beruntung <br>', '2015-04-29 01:11:11', 1),
(60, 'asda', 'Buku GRATIS !!!<span> Teman-teman saya mau bagi buku gratis nih, syarat nya:<br> 1. Like dan share status ini<br> 2. Invite pin saya 53DEA7F1<span><br> 3. Silakan tulis di kolom komentar dengan impian terbesar teman-teman</span></span> Karena buku nya tinggal satu pcs, maka komentar terakhir yang mendapatkan like dari saya yang akan mendapatkan buku ini. Pemenang akan saya umumkan besok siang<br> Selamat mencoba, semoga anda beruntung <br>', '2015-04-29 01:11:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `id` int(11) NOT NULL,
  `tanggal_upload` date NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `tipe_file` varchar(10) NOT NULL,
  `ukuran_file` varchar(20) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id`, `tanggal_upload`, `nama_file`, `tipe_file`, `ukuran_file`, `file`) VALUES
(6, '2015-04-28', 'sdf', 'pdf', '128978', 'files/sdf.pdf'),
(8, '2015-04-28', 'daasd', 'docx', '59103', 'files/daasd.docx'),
(9, '2015-04-28', 'asdasdasdas', 'docx', '59103', 'files/asdasdasdas.docx'),
(10, '2015-05-04', 'o9o', 'pdf', '1213253', 'files/o9o.pdf'),
(11, '2015-05-08', 'asd', 'pdf', '373321', 'files/asd.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `galeri_id` int(11) NOT NULL,
  `galeri_nama` varchar(100) NOT NULL,
  `galeri_keterangan` text NOT NULL,
  `galeri_link` text NOT NULL,
  `galeri_tgl` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`galeri_id`, `galeri_nama`, `galeri_keterangan`, `galeri_link`, `galeri_tgl`) VALUES
(31, 'Ary', 'Ary', 'uploads/C360_2015-01-03-14-20-25-428.jpg', '0000-00-00 00:00:00'),
(32, 'asd', 'asd', 'uploads/C360_2015-01-03-14-20-52-982.jpg', '0000-00-00 00:00:00'),
(34, 'ggg', 'ggg', 'uploads/C360_2015-01-03-15-51-19-682.jpg', '0000-00-00 00:00:00'),
(36, 's', 'ss', 'uploads/C360_2015-01-03-15-50-31-526.jpg', '0000-00-00 00:00:00'),
(38, 'dfsdf', 'fsdf', 'uploads/C360_2014-07-13-19-27-14-435.jpg', '0000-00-00 00:00:00'),
(39, 'dsfsd', 'fsfsdfsdf', 'uploads/20140826_090837.jpg', '0000-00-00 00:00:00'),
(40, 'bbb', 'bb', 'uploads/20140826_091057.jpg', '0000-00-00 00:00:00'),
(41, 'ASdasdasd', 'asdasd', 'uploads/apple_vnbwfjei.jpg', '2015-03-27 23:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(50) NOT NULL,
  `kategori_deskripsi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_nama`, `kategori_deskripsi`) VALUES
(1, 'Hot News', 'Berisikan Berita Terpanas'),
(2, 'School News', 'Berisikan Berita Terupdate Sekolah'),
(3, 'Puisi', 'Tulisan Bertemakan Puisi'),
(4, 'Cerpen', 'Kumpulan Cerita Pendek');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `kelas_id` int(11) NOT NULL,
  `kelas_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kelas_id`, `kelas_nama`) VALUES
(1, 'X-1'),
(2, 'X-2'),
(3, 'X-3'),
(4, 'X-4'),
(5, 'X-5'),
(6, 'X-6');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `nilai_id` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `pelajaran_id` int(11) DEFAULT NULL,
  `tahun_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `nilai_kkm` int(11) DEFAULT NULL,
  `nilai_poin` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nilai_id`, `id`, `pelajaran_id`, `tahun_id`, `semester_id`, `nilai_kkm`, `nilai_poin`) VALUES
(136, 27, 1, 1, 1, 80, 77),
(137, 28, 1, 1, 1, 80, 80),
(138, 29, 1, 1, 1, 80, 90),
(139, 30, 1, 1, 1, 80, 90),
(140, 27, 4, 1, 1, 80, 80),
(141, 28, 4, 1, 1, 80, 90),
(142, 29, 4, 1, 1, 80, 80),
(143, 30, 4, 1, 1, 80, 90),
(144, 27, 5, 1, 1, 80, 90),
(145, 28, 5, 1, 1, 80, 90),
(146, 29, 5, 1, 1, 80, 90),
(147, 30, 5, 1, 1, 80, 90),
(148, 27, 6, 1, 1, 90, 90),
(149, 28, 6, 1, 1, 90, 90),
(150, 29, 6, 1, 1, 90, 80),
(151, 30, 6, 1, 1, 90, 77),
(152, 27, 7, 1, 1, 90, 77),
(153, 28, 7, 1, 1, 90, 80),
(154, 29, 7, 1, 1, 90, 80),
(155, 30, 7, 1, 1, 90, 90),
(156, 27, 8, 1, 1, 90, 90),
(157, 28, 8, 1, 1, 90, 90),
(158, 29, 8, 1, 1, 90, 80),
(159, 30, 8, 1, 1, 90, 77),
(160, 27, 9, 1, 1, 70, 90),
(161, 28, 9, 1, 1, 70, 77),
(162, 29, 9, 1, 1, 70, 80),
(163, 30, 9, 1, 1, 70, 80),
(164, 27, 10, 1, 1, 75, 90),
(165, 28, 10, 1, 1, 75, 77),
(166, 29, 10, 1, 1, 75, 80),
(167, 30, 10, 1, 1, 75, 90),
(168, 27, 11, 1, 1, 70, 90),
(169, 28, 11, 1, 1, 70, 80),
(170, 29, 11, 1, 1, 70, 80),
(171, 30, 11, 1, 1, 70, 90),
(172, 27, 12, 1, 1, 75, 80),
(173, 28, 12, 1, 1, 75, 90),
(174, 29, 12, 1, 1, 75, 77),
(175, 30, 12, 1, 1, 75, 80),
(176, 27, 13, 1, 1, 90, 90),
(177, 28, 13, 1, 1, 90, 90),
(178, 29, 13, 1, 1, 90, 80),
(179, 30, 13, 1, 1, 90, 80),
(180, 27, 14, 1, 1, 70, 90),
(181, 28, 14, 1, 1, 70, 90),
(182, 29, 14, 1, 1, 70, 80),
(183, 30, 14, 1, 1, 70, 77),
(184, 27, 15, 1, 1, 70, 90),
(185, 28, 15, 1, 1, 70, 80),
(186, 29, 15, 1, 1, 70, 77),
(187, 30, 15, 1, 1, 70, 76),
(188, 27, 16, 1, 1, 70, 90),
(189, 28, 16, 1, 1, 70, 80),
(190, 29, 16, 1, 1, 70, 77),
(191, 30, 16, 1, 1, 70, 80);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_uas`
--

CREATE TABLE IF NOT EXISTS `nilai_uas` (
  `uas_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `pelajaran_id` int(11) NOT NULL,
  `tahun_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `nilai_kkm` int(11) NOT NULL,
  `nilai_poin` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_uas`
--

INSERT INTO `nilai_uas` (`uas_id`, `id`, `pelajaran_id`, `tahun_id`, `semester_id`, `nilai_kkm`, `nilai_poin`) VALUES
(136, 27, 1, 1, 1, 80, 77),
(137, 28, 1, 1, 1, 80, 80),
(138, 29, 1, 1, 1, 80, 90),
(139, 30, 1, 1, 1, 80, 90),
(140, 27, 4, 1, 1, 80, 80),
(141, 28, 4, 1, 1, 80, 90),
(142, 29, 4, 1, 1, 80, 80),
(143, 30, 4, 1, 1, 80, 90),
(144, 27, 5, 1, 1, 80, 90),
(145, 28, 5, 1, 1, 80, 90),
(146, 29, 5, 1, 1, 80, 90),
(147, 30, 5, 1, 1, 80, 90),
(148, 27, 6, 1, 1, 90, 90),
(149, 28, 6, 1, 1, 90, 90),
(150, 29, 6, 1, 1, 90, 80),
(151, 30, 6, 1, 1, 90, 77),
(152, 27, 7, 1, 1, 90, 77),
(153, 28, 7, 1, 1, 90, 80),
(154, 29, 7, 1, 1, 90, 80),
(155, 30, 7, 1, 1, 90, 90),
(156, 27, 8, 1, 1, 90, 90),
(157, 28, 8, 1, 1, 90, 90),
(158, 29, 8, 1, 1, 90, 80),
(159, 30, 8, 1, 1, 90, 77),
(160, 27, 9, 1, 1, 70, 90),
(161, 28, 9, 1, 1, 70, 77),
(162, 29, 9, 1, 1, 70, 80),
(163, 30, 9, 1, 1, 70, 80),
(164, 27, 10, 1, 1, 75, 90),
(165, 28, 10, 1, 1, 75, 77),
(166, 29, 10, 1, 1, 75, 80),
(167, 30, 10, 1, 1, 75, 90),
(168, 27, 11, 1, 1, 70, 90),
(169, 28, 11, 1, 1, 70, 80),
(170, 29, 11, 1, 1, 70, 80),
(171, 30, 11, 1, 1, 70, 90),
(172, 27, 12, 1, 1, 75, 80),
(173, 28, 12, 1, 1, 75, 90),
(174, 29, 12, 1, 1, 75, 77),
(175, 30, 12, 1, 1, 75, 80),
(176, 27, 13, 1, 1, 90, 90),
(177, 28, 13, 1, 1, 90, 90),
(178, 29, 13, 1, 1, 90, 80),
(179, 30, 13, 1, 1, 90, 80),
(180, 27, 14, 1, 1, 70, 90),
(181, 28, 14, 1, 1, 70, 90),
(182, 29, 14, 1, 1, 70, 80),
(183, 30, 14, 1, 1, 70, 77),
(184, 27, 15, 1, 1, 70, 90),
(185, 28, 15, 1, 1, 70, 80),
(186, 29, 15, 1, 1, 70, 77),
(187, 30, 15, 1, 1, 70, 76),
(188, 27, 16, 1, 1, 70, 90),
(189, 28, 16, 1, 1, 70, 80),
(190, 29, 16, 1, 1, 70, 77),
(191, 30, 16, 1, 1, 70, 80);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_uts`
--

CREATE TABLE IF NOT EXISTS `nilai_uts` (
  `uts_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `pelajaran_id` int(11) NOT NULL,
  `tahun_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `nilai_kkm` int(11) NOT NULL,
  `nilai_poin` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_uts`
--

INSERT INTO `nilai_uts` (`uts_id`, `id`, `pelajaran_id`, `tahun_id`, `semester_id`, `nilai_kkm`, `nilai_poin`) VALUES
(136, 27, 1, 1, 1, 80, 77),
(137, 28, 1, 1, 1, 80, 80),
(138, 29, 1, 1, 1, 80, 90),
(139, 30, 1, 1, 1, 80, 90),
(140, 27, 4, 1, 1, 80, 80),
(141, 28, 4, 1, 1, 80, 90),
(142, 29, 4, 1, 1, 80, 80),
(143, 30, 4, 1, 1, 80, 90),
(144, 27, 5, 1, 1, 80, 90),
(145, 28, 5, 1, 1, 80, 90),
(146, 29, 5, 1, 1, 80, 90),
(147, 30, 5, 1, 1, 80, 90),
(148, 27, 6, 1, 1, 90, 90),
(149, 28, 6, 1, 1, 90, 90),
(150, 29, 6, 1, 1, 90, 80),
(151, 30, 6, 1, 1, 90, 77),
(152, 27, 7, 1, 1, 90, 77),
(153, 28, 7, 1, 1, 90, 80),
(154, 29, 7, 1, 1, 90, 80),
(155, 30, 7, 1, 1, 90, 90),
(156, 27, 8, 1, 1, 90, 90),
(157, 28, 8, 1, 1, 90, 90),
(158, 29, 8, 1, 1, 90, 80),
(159, 30, 8, 1, 1, 90, 77),
(160, 27, 9, 1, 1, 70, 90),
(161, 28, 9, 1, 1, 70, 77),
(162, 29, 9, 1, 1, 70, 80),
(163, 30, 9, 1, 1, 70, 80),
(164, 27, 10, 1, 1, 75, 90),
(165, 28, 10, 1, 1, 75, 77),
(166, 29, 10, 1, 1, 75, 80),
(167, 30, 10, 1, 1, 75, 90),
(168, 27, 11, 1, 1, 70, 90),
(169, 28, 11, 1, 1, 70, 80),
(170, 29, 11, 1, 1, 70, 80),
(171, 30, 11, 1, 1, 70, 90),
(172, 27, 12, 1, 1, 75, 80),
(173, 28, 12, 1, 1, 75, 90),
(174, 29, 12, 1, 1, 75, 77),
(175, 30, 12, 1, 1, 75, 80),
(176, 27, 13, 1, 1, 90, 90),
(177, 28, 13, 1, 1, 90, 90),
(178, 29, 13, 1, 1, 90, 80),
(179, 30, 13, 1, 1, 90, 80),
(180, 27, 14, 1, 1, 70, 90),
(181, 28, 14, 1, 1, 70, 90),
(182, 29, 14, 1, 1, 70, 80),
(183, 30, 14, 1, 1, 70, 77),
(184, 27, 15, 1, 1, 70, 90),
(185, 28, 15, 1, 1, 70, 80),
(186, 29, 15, 1, 1, 70, 77),
(187, 30, 15, 1, 1, 70, 76),
(188, 27, 16, 1, 1, 70, 90),
(189, 28, 16, 1, 1, 70, 80),
(190, 29, 16, 1, 1, 70, 77),
(191, 30, 16, 1, 1, 70, 80);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `pelajaran_id` int(11) NOT NULL,
  `pelajaran_nama` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`pelajaran_id`, `pelajaran_nama`) VALUES
(1, 'Matematika'),
(4, 'Bahasa Indonesia'),
(5, 'Bahasa Inggris'),
(6, 'Pendidikan Agama'),
(7, 'Pendidikan Kewarganegaraan'),
(8, 'Ilmu Pengetahuan Alam'),
(9, 'Ilmu Pengetahuan Sosial'),
(10, 'Seni Budaya'),
(11, 'Pendidikan Jasmani, Olahraga, dan Kesehatan'),
(12, 'Ketrampilan'),
(13, 'Teknologi Informasi dan Komunikasi'),
(14, 'Bahasa dan Sastra Sunda'),
(15, 'PLH'),
(16, 'Tata Busana');

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE IF NOT EXISTS `sekolah` (
  `sekolah_id` int(11) NOT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `sekolah_alamat` text,
  `sekolah_telp` varchar(16) DEFAULT NULL,
  `sekolah_visi` text,
  `sekolah_misi` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`sekolah_id`, `sekolah_nama`, `sekolah_alamat`, `sekolah_telp`, `sekolah_visi`, `sekolah_misi`) VALUES
(1, 'SMP Negeri 2 Sukaratu', 'Jalan Sukamanah Gunungsari Sukaratu\r\nKabupaten Tasikmalaya\r\nKode Pos 46152', '021-909090', 'Terciptanya sekolah ramah anak, unggul dalam prestasi, berkarakter,berakar pada budaya bangsa, dan berwawasan lingkungan, berlandaskan IMTAQ dan IPTEK.', '1.&nbsp;&nbsp;&nbsp; Melaksanakan pembelajaran dan bimbingan secara efektif dan kompetitif<br>2.&nbsp;&nbsp;&nbsp; Mendorong dan membantu siswa untuk mengenali potensi dirinya sehingga dapat dikembangkan secara optimal<br>3.&nbsp;&nbsp;&nbsp; Menumbuhkan semangat keunggulan secara intensif kepada seluruh warga sekolah<br>4.&nbsp;&nbsp;&nbsp; Membudayakan kegiatan 7 S yaitu senyum, salam, sapa, sopan, santun, semangat dan sepenuh hati&nbsp; pada seluruh warga sekolah<br>5.&nbsp;&nbsp;&nbsp; Menumbuhkan dan&nbsp; melestarikan budaya lokal.<br>6.&nbsp;&nbsp;&nbsp; Menumbuhkan penghayatan terhadap ajaran agama yang dianut sebagai landasan kearifan lokal dalam bergaul dan bertindak.<br>7.&nbsp;&nbsp;&nbsp; Mengembangkan mutu kelembagaan dan manajemen.');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `semester_id` int(11) NOT NULL,
  `semester_nama` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`semester_id`, `semester_nama`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE IF NOT EXISTS `tahun` (
  `tahun_id` int(11) NOT NULL,
  `tahun_nama` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`tahun_id`, `tahun_nama`) VALUES
(1, '2014/2015'),
(2, '2016/2017');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `nomor_induk` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `telp` varchar(16) DEFAULT NULL,
  `alamat` text,
  `status` varchar(25) DEFAULT NULL,
  `jenis_kelamin` varchar(25) DEFAULT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `access` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nomor_induk`, `name`, `username`, `password`, `telp`, `alamat`, `status`, `jenis_kelamin`, `kelas_id`, `access`) VALUES
(10, NULL, 'Administrator', 'admin', 'admin', '085761167894', 'Padang', 'PNS', 'Perempuan', NULL, 'admin'),
(25, '56', 'siswa', 'siswa', 'siswa', '456', 'frghf', 'PNS', 'Perempuan', 4, 'siswa'),
(26, 'op', 'guru', 'guru', 'guru', 'op', 'op', 'Honorer', 'Laki-laki', 1, 'guru'),
(27, '1', 'Ary', 'ary', 'ary', '123422', 'asdas bbb', 'dasdas', 'asdas', 1, 'siswa'),
(28, '675675', 'entis', 'entis', 'sd', '3453', 'jalan', 'Aktif', 'Laki-laki', 1, 'siswa'),
(29, '345', 'umar', 'umar', 'sd', '3453', 'jl', 'Aktif', 'Laki-laki', 1, 'siswa'),
(30, '111111', 'aziz', 'aziz', 'aziz', '089672825878', 'jl', NULL, 'Laki-laki', 1, 'siswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD UNIQUE KEY `id_absen` (`id_absen`) COMMENT 'primary key';

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`artikel_id`), ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`galeri_id`), ADD UNIQUE KEY `galeri_nama` (`galeri_nama`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kelas_id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`nilai_id`), ADD KEY `siswa_id` (`id`), ADD KEY `pelajaran_id` (`pelajaran_id`), ADD KEY `tahunajaran_id` (`tahun_id`), ADD KEY `semester_id` (`semester_id`);

--
-- Indexes for table `nilai_uas`
--
ALTER TABLE `nilai_uas`
  ADD PRIMARY KEY (`uas_id`);

--
-- Indexes for table `nilai_uts`
--
ALTER TABLE `nilai_uts`
  ADD PRIMARY KEY (`uts_id`);

--
-- Indexes for table `pelajaran`
--
ALTER TABLE `pelajaran`
  ADD PRIMARY KEY (`pelajaran_id`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`sekolah_id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`semester_id`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`tahun_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD KEY `kelas_id` (`kelas_id`), ADD KEY `kelas_id_2` (`kelas_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `id_absen` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `artikel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `galeri_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kelas_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `nilai_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `nilai_uas`
--
ALTER TABLE `nilai_uas`
  MODIFY `uas_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `nilai_uts`
--
ALTER TABLE `nilai_uts`
  MODIFY `uts_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `pelajaran`
--
ALTER TABLE `pelajaran`
  MODIFY `pelajaran_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `sekolah_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `semester_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `tahun_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
