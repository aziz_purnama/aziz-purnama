<?php 
    session_start();
    require_once('../../config/db.php');
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Laporan Nilai Perkelas</title>
        <body>
            <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tgh  {border-collapse:collapse;border-spacing:0;border-color:white;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>
            <?php
                $kelas ="";
                $nama ="";
                $nim ="";
                $semester ="";
                $ta ="";
                $smt = mysql_query("select * from semester where semester_id = $_GET[semester]");
                $thn = mysql_query("select * from tahun where tahun_id = $_GET[tahun]"); 
                while ($th=mysql_fetch_array($thn)) {
                     $ta = $th['tahun_nama'];
                }
                while ($sm=mysql_fetch_array($smt)) {
                     $semester = $sm['semester_nama'];
                }
                $users = mysql_query("select users.name,users.nomor_induk, kelas.kelas_nama from users 
                                    LEFT JOIN kelas on kelas.kelas_id = users.kelas_id
                                    where users.id = $_GET[siswa]"); 
                while ($data0=mysql_fetch_array($users)) {
                     $kelas = $data0['kelas_nama'];
                     $nama = $data0['name'];
                     $nim = $data0['nomor_induk'];
                }
            ?>
            <div style="font-family:Arial; font-size:12px;">
                <center><h3><u>DAFTAR NILAI RAPOT</u></h3></center>
                <center><h3>KURIKULKUM 2006 (KTSP)</h3></center> 
            </div>
            <br>
            <table style = "font-size: 12px;">
                <tr>
                    <td>Nama Sekolah</td>
                    <td>:</td>
                    <td>SMP NEGRI 2 SUKARATU</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Kelas</td>
                    <td>:</td>
                    <td><?php echo $kelas;?></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>Jalan Sukamanah Desa Gunungsari Kec. Sukaratu Kab. Tasikmalaya 46152</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Semester</td>
                    <td>:</td>
                    <td><?php echo $semester;?></td>
                </tr>
                <tr>
                    <td>Nama Peserta Didik</td>
                    <td>:</td>
                    <td><?php echo $nama;?></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>Tahun Pelajaran</td>
                    <td>:</td>
                    <td><?php echo $ta;?></td>
                </tr>
                <tr>
                    <td>Nomor Induk</td>
                    <td>:</td>
                    <td><?php echo $nim;?></td>
                    <td></td>
                </tr>
            </table>
            <table class="tg">
            <tr>
                <th class="tg-3wr7"  rowspan="2">No</th>
                <th class="tg-3wr7"  rowspan="2">Mata Pelajaran</th>
                <th class="tg-3wr7"  rowspan="2">KKM*)</th>
                <th class="tg-3wr7" colspan="2">Nilai</th>
                <th class="tg-3wr7"  rowspan="2">Deskripsi Kemajuan Belajar</th>
            </tr>
            <tr>
                <th>Angka</th>
                <th>Huruf</th>
            </tr>
            <?php
            //$pelajaran  =   $_POST['pelajaran'];
            
            $no     =   1; 
            // $nilai1  =   mysql_query("SELECT pelajaran.pelajaran_nama,nilai.nilai_id, nilai.nilai_kkm, users.name, kelas.kelas_nama, pelajaran.pelajaran_nama, semester.semester_nama, tahun.tahun_nama, nilai.nilai_poin 
            //                     FROM nilai 
            //                     INNER JOIN users ON nilai.id=users.id 
            //                     INNER JOIN kelas ON users.kelas_id=kelas.kelas_id 
            //                     INNER JOIN pelajaran ON nilai.pelajaran_id=pelajaran.pelajaran_id 
            //                     INNER JOIN semester ON nilai.semester_id=semester.semester_id 
            //                     INNER JOIN tahun ON nilai.tahun_id=tahun.tahun_id 
            //                     WHERE kelas.kelas_id=$_GET[kelas] AND semester.semester_id=$_GET[semester] AND tahun.tahun_id=$_GET[tahun]
            //                     ORDER BY users.name ASC");
            $izin = "";
            $sakit = "";
            $alfa ="";
            
            $nilai1  =   mysql_query("SELECT users.name, pelajaran.pelajaran_nama, nilai.nilai_kkm, nilai.nilai_poin
                                FROM pelajaran 
                                LEFT JOIN nilai on pelajaran.pelajaran_id=nilai.pelajaran_id
                                LEFT JOIN users on nilai.id = users.id
                                INNER JOIN semester ON nilai.semester_id=semester.semester_id 
                                INNER JOIN tahun ON nilai.tahun_id=tahun.tahun_id 
                                WHERE semester.semester_id=$_GET[semester] AND tahun.tahun_id=$_GET[tahun] AND users.id=$_GET[siswa]
                                ORDER BY users.name ASC");
            $izin1 = mysql_query("SELECT COUNT(id_absen), izin FROM absen
                                    WHERE nis = $_GET[siswa] AND izin = 1
                                    GROUP BY izin");   
            while ($data1=mysql_fetch_array($izin1)) {
                 $izin = $data1['COUNT(id_absen)'];
            }
            $sakit1 = mysql_query("SELECT COUNT(id_absen), sakit FROM absen
                                    WHERE nis = $_GET[siswa] AND sakit = 1
                                    GROUP BY sakit");   
            while ($data2=mysql_fetch_array($sakit1)) {
                 $sakit = $data2['COUNT(id_absen)'];
            }
            $alfa1 = mysql_query("SELECT COUNT(id_absen), alfa FROM absen
                                    WHERE nis = $_GET[siswa] AND alfa = 1
                                    GROUP BY alfa");   
            while ($data3=mysql_fetch_array($alfa1)) {
                 $alfa = $data3['COUNT(id_absen)'];
            }
           
            while ($data=mysql_fetch_array($nilai1)) {
            ?>
            <tr>
                <td class="tg-rv4w" width="10%"><?php echo $no; ?></td>
                <td class="tg-rv4w" width="90%" ><?php echo $data['pelajaran_nama']; ?></td>
                <td class="tg-rv4w" width="90%"><?php echo $data['nilai_kkm']; ?></td>
                <td class="tg-rv4w" width="90%"><?php echo $data['nilai_poin']; ?></td>
                <td class="tg-rv4w" width="90%"></td>
                <?php
                    if ($data['nilai_kkm'] <= $data['nilai_poin']) {
                ?>
                <td class="tg-rv4w" width="90%">Tuntas</td>
                <?php
                     }else{
                ?>
                <td class="tg-rv4w" width="90%">Tidak Tuntas</td>
                <?php
                     } 
                ?>
            </tr>
            <?php
                $no++;
                }                                            
            ?>  
            </table><br/>
            <table class="tg">
                <tr>
                    <td><center>Kegiatan Pengembangan Diri</center></td>
                    <td><center>Nilai</center></td>
                    <td><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Keterangan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center></td>
                </tr>
                <tr>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                </tr>
                <tr>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                </tr>
                <tr>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                    <td style="padding: 8;"></td>
                </tr>
            </table><br/>
            <table class="tg">
                <tr>
                    <td><center>&nbsp;&nbsp;Akhlak dan Kepribadian&nbsp;&nbsp;</center></td>
                    <td rowspan="4" style="border-top:white;border-bottom:white"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center></td>
                    <td><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ketidakhadiran &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center></td>
                </tr>
                <tr>
                    <td style="padding: 3;">Akhlak &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                    <td style="padding: 3;">1. Sakit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $sakit; ?></td>
                </tr>
                <tr>
                    <td style="padding: 3;">Kepribadian :</td>
                    <td style="padding: 3;">2. Izin &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $izin; ?></td>
                </tr>
                <tr>
                    <td style="padding: 3;">Rangking &nbsp;&nbsp;&nbsp;&nbsp;:</td>
                    <td style="padding: 3;">3. Tanpa Keterangan &nbsp;&nbsp;: <?php echo $alfa; ?></td>
                </tr>
            </table><br/>
            <table class="tgh" style="font-size:10px"> 
                <tr>
                    <td rowspan="2"><center>Orang Tua/Wali</center></td>
                    <td><center>Diketahui</center></td>
                    <td><center>Tasikmalaya, <?php echo date("d F Y"); ?></center></td>
                </tr>
                <tr>
                    <td><center>Kepala Sekolah,</center></td>
                    <td><center>Wali Kelas,</center></td>
                </tr>
            </table><br/><br/><br/><br/>
            <table class="tgh" style="font-size:10px"> 
                <tr>
                    <td><center>...............................</center></td>
                    <td><center><u>DENDIN WARDIANA. S.Pd., M.Pd.</u></center></td>
                    <td><center><u>DADANG HUDAN DARDIRI, S.Pd,. M.Pd.</u></center></td>
                </tr>
                <tr>
                    <td></td>
                    <td><center>NIP. </center></td>
                    <td><center>NIP. </center></td>
                </tr>
            </table>
        </body>
    </head>
</html>